// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import "package:hovering/hovering.dart";

// This is a show dialog widget that shows licenses and app information
Widget my_custom_showdailog_widget(
        BuildContext context,
        String legalese,
        String name_of_app,
        String version,
        List<Widget> children,
        Icon app_icon,
        Widget button_label) =>
    ElevatedButton(
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) => AboutDialog(
                  applicationIcon: app_icon,
                  applicationLegalese: legalese,
                  applicationName: name_of_app,
                  applicationVersion: version,
                  children: children,
                ));
      },
      child: button_label,
    );

Widget my_custom_alertdialog_widget(BuildContext context,
String alert_title, String title, double padding_value)
  => ElevatedButton(
      child: const Text("Show Alert Dialog"),
      onPressed: () {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(alert_title),
              ),
            ],
            title: Text(title),
            contentPadding: EdgeInsets.all(padding_value),
          ),
        );
      },
    );

// This is a web package to enable the hover effect on Flutter web.
//HoverWidget changes one widget to another on Hover Also uses onHover property
//
Widget my_custom_web_hover_widget(BuildContext context, 
Widget child, Widget hoverchild, Function event) 
  =>  HoverWidget(
    hoverChild: hoverchild,
    onHover: (event) {},
    child: child,
  );

//Hover effect on a Container
Widget my_custom_web_hover_container(BuildContext context) => HoverContainer();

//Gives a Hover effect with a fade animation.
//Transition between two widgets.
Widget my_custom_web_hover_crossfade_container(BuildContext context,
Duration duration, Widget fist_child, Widget second_child) =>
    HoverCrossFadeWidget(
      duration: duration,
      firstChild: fist_child,
      secondChild: second_child,
    );


// Creates a Container widget with an Animation.
// Duration can be provided.
Widget my_custom_web_hover_animated_container(BuildContext context)
  => HoverAnimatedContainer(

    );


// Creates a Button widget with hover effect.
Widget my_custom_web_hover_button(BuildContext context, 
Function onPressed) 
  => HoverButton(
      onpressed: (){}
    );

